<?php
require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use App\Model\Category;

$category = Category::find(2);
$category->delete();